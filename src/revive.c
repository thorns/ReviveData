#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#define ARRAY_ELEMS(x) (sizeof(x) / sizeof(*x))

static const char *vars[] = {
    "ADMBASE::gxx",
    "ADMBASE::gyy",
    "ADMBASE::gzz",
    "ADMBASE::gxy",
    "ADMBASE::gxz",
    "ADMBASE::gyz",
    "ADMBASE::kxx",
    "ADMBASE::kyy",
    "ADMBASE::kzz",
    "ADMBASE::kxy",
    "ADMBASE::kxz",
    "ADMBASE::kyz",
    "ADMBASE::alp",
    "ADMBASE::betax",
    "ADMBASE::betay",
    "ADMBASE::betaz",
};

void revive_data(CCTK_ARGUMENTS)
{
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    double x0, z0, dx, dz;
    int dummy;
    size_t grid_size = cctk_lsh[2] * cctk_lsh[0];

    x0 = x[CCTK_GFINDEX3D(cctkGH, 0, 0, 0)];
    z0 = z[CCTK_GFINDEX3D(cctkGH, 0, 0, 0)];
    dx = x[CCTK_GFINDEX3D(cctkGH, 1, 0, 0)] - x0;
    dz = z[CCTK_GFINDEX3D(cctkGH, 0, 0, 1)] - z0;

    for (int i = 0; i < ARRAY_ELEMS(vars); i++) {
        double *dst = CCTK_VarDataPtr(cctkGH, 0, vars[i]);
        char popen_cmd_buf[1024];
        int ret;
        FILE *child_stream;

        snprintf(popen_cmd_buf, sizeof(popen_cmd_buf),
                 "%s %s %16.16g %d "
                 "%s %16.16g %16.16g %16.16g %16.16g %d %d",
                 load_data_exe, simdata_path, load_time, load_iteration,
                 vars[i], x0, z0, dx, dz, cctk_lsh[0], cctk_lsh[2]);
        fprintf(stderr, "%s\n", popen_cmd_buf);

        child_stream = popen(popen_cmd_buf, "r");
        if (!child_stream) {
            fprintf(stderr, "Error starting child process '%s': %s\n",
                    popen_cmd_buf, strerror(errno));
            abort();
        }

        ret = fread(dst, sizeof(*dst), grid_size, child_stream);
        if (ret < grid_size) {
            fprintf(stderr, "Not enough items read for var %s: %d\n",
                    vars[i], ret);
            abort();
        }

        /* check that there is nothing more to read */
        ret = fread(&dummy, sizeof(dummy), 1, child_stream);
        if (ret > 0 || !feof(child_stream)) {
            fprintf(stderr, "Extra data in input for var %s\n", vars[i]);
            abort();
        }
        pclose(child_stream);
    }
}
